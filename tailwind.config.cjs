const config = {
    mode: 'jit',
    purge: ['./src/**/*.{js,jsx,ts,tsx}'],

    theme: {
        extend: {},
    },

    plugins: [
        require('daisyui')
    ],
};

module.exports = config;
